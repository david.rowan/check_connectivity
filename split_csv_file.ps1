<#

.SYNOPSIS
Split a big csv file into multipl files of no more than 2000 entries

.PARAMETER inputFile
The name of the file that you want to split

.PARAMETER size
The number of lines that you want in each new file

.PARAMETER raw
Treats the file as text, not csv (so doesn't put a header in or wrap entries in "")

.EXAMPLE
.\split_csv_file.ps1 -inputFile aBigCSVFile.csv
Splits aBigCSVFile.csv into different portions 2000 entries long

.EXAMPLE
./split_csv_file.ps1 -inputFile aBigCSVFile.csv -size 500
SplitsaBigCSVFile.csv into different portions 500 entries long

#>

Param
(
    [Parameter(Mandatory=$true, Position=0)]
    [string] $inputFile,
    [Parameter(Mandatory = $false, Position = 1)]
    [int] $size = 2000,
    [Parameter(Mandatory = $false, Position = 2)]
    [switch] $raw
)

# check if the file exists

# import the object
$objectToSplit = Import-Csv $inputFile
if ($raw) { $objectToSplit = Get-Content $inputFile}

if ($objectToSplit.Count -le $size) 
{
    # object is already smaller than requested. Exit.
    exit -1
}

# get the number of files that this will be split into
$noOfFiles = [math]::ceiling($objectToSplit.Count / $size)

# set up counting and filename index
$fileIndex = 1

# go into my while loop
while ($fileIndex -le $noOfFiles)
{
    $newSegment = @()
    $rest = @()
    $firstElement = ""
    while (($newSegment.Length -lt $size) -and ($objectToSplit.Length -gt 0))
    {
        # From: https://stackoverflow.com/questions/24754822/powershell-remove-item-0-from-an-array
        $firstElement, $rest = $objectToSplit
        $newSegment += $firstElement
        $objectToSplit = $rest
    }

    # construct a filename
    $filename = $inputFile.Substring(0,($inputFile.Length - 4)) + "_file_" + $fileIndex + "_of_" + $noOfFiles + ".csv"
    
    # export to the file
    $newSegment | Export-Csv -Path $filename
    if ($raw) { $newSegment | Out-File $filename }
    $newSegment = @()

    # increment the filename index
    $fileIndex++
}
