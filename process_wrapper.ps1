<#
    .SYNOPSIS
    Takes the list of computers and goes through each one remoting to it testing to see if connectivity is possible on the host/protocol/port from the csv file.

    .DESCRIPTION
    Takes the list of computers and goes through each one remoting to it testing to see if connectivity is possible on the host/protocol/port from the csv file.

    .EXAMPLE
    .\process_wrapper.ps1 -computerNameFile listOfComputers.txt -outputFile finalResults.csv 

    .PARAMETER computerNameFile
    A file containg a list of computernames, one per line

    .PARAMETER outputFile
    A location to save the final csv file

    .PARAMETER vulnerabilityFile
    The location of the first vulnerability file to work on. If this is not input0.csv then you'll need to change the script.
    This is here for future use. Best not to mess with it now.

    .OUTPUTS
    Outputs a csv file "finalResults" (our outputFile specified).
    Along the way it creates temporary csv files which are not deleted so it's important to use the cleanup script afterwards.

#>

Param
(
[Parameter(Mandatory=$false, Position=0)]
[string] $computerNameFile = "C:\Users\davidr2\Documents\scripts\listOfComputers.txt",
[Parameter(Mandatory=$false, Position=1)]
[string] $outputFile = "C:\Users\davidr2\Documents\scripts\finalResults.csv",
[Parameter(Mandatory=$false, Position=2)]
[string] $vulnerabilityFile = "C:\Users\davidr2\Documents\scripts\input0.csv"
)


# Get the list of computers
$listOfComputers = Get-Content $computerNameFile

# Get the remote script from git
$myCommand = (New-Object System.Net.WebClient).DownloadString('https://git.app.uib.no/david.rowan/check_connectivity/-/raw/main/check_connectivity_function_for_remote.ps1?inline=false')


# loop round each of the computers
for ($i = 0; $i -lt $listOfComputers.Length; $i++)
{
    $myInputObjectFileName = "input" + $i + ".csv"
    $outputIndex = $i + 1
    $myOutputFileName = "input" + $outputIndex + ".csv"
    $myInputObject = Import-csv -Path $myInputObjectFileName
    if (Test-Connection -ComputerName $listOfComputers[$i] -Quiet) 
    {
	    write-output ("Checking on {)}" -f $listOfComputers[$i])
        $myResult = ""
        $myResult = Invoke-Command -ComputerName $listOfComputers[$i] -scriptblock { Invoke-Expression $using:myCommand; $result = Test-Many-ports -inputObject $using:myInputObject; return $result }

        # write the results to a new csv so that if it fails we at least can get interim results from each run...
        $myResult | Export-Csv -Path $myOutputFileName -NoTypeInformation
    }
    else 
    {
        # if we can't connect to the computer just copy the file as a hack workaround
	    write-output ("Couldn't connect. Skipping {0}" -f $listOfComputers[$i])
        Copy-Item $myInputObjectFileName -Destination $myOutputFileName
    }
    Copy-Item $myOutputFileName -Destination $outputFile -Force
}
