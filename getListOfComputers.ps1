<#
    .SYNOPSIS
    Get a list of computers from various subnets specified.
    Save to a file a list of one per subnet that could be connected to.

    .DESCRIPTION
    Takes a list of subnets in the form of a file (one subnet prefix per line, see example) and searches using azure for windows computers that are on those subnets.
    Returns a file that includes one computer from each subnet where we were able to connect to them. These will be Windows computers only on the basis that this is a list of computers we want to remote to

    .EXAMPLE
    ./getListOfComputers.ps1 -inputFile subnetList.txt -outputFile listOfComputers.txt

    .PARAMETER inputFile
    A file containg a list of subnet prefixes (e.g. 192.168.1. for a 192.168.1.0/22 network)

    .PARAMETER outputFile
    The output file to which the list of computers will be written. WILL OVERWRITE THE FILE IF IT ALREADY EXISTS!

    .OUTPUTS
    Will output some text along the way if it can't find any hosts in a given subnet.
    Return value is the number of hosts that have been found and saved to the output file

    TODO:
        Consider whether it would be better to return the list of computers instead of a return value.

    #>

Param
    (
    [Parameter(Mandatory=$false, Position=0)]
    [string] $inputFile = "C:\Users\davidr\Documents\scripts\check_connectivity\subnetList.txt",
    [Parameter(Mandatory=$false, Position=1)]
    [string] $outputFile = "C:\Users\davidr\Documents\scripts\check_connectivity\listOfComputers.txt"
    )
        

# get list of subnets
$subnetList = Get-Content $inputFile

#Last inn appinfo

# appinfo.xml is created using Get-Credential | Export-CliXML appinfo.xml
# use tennantID as the username and appID as the password
$appinfo = Import-Clixml appinfo.xml
$tenantid = $appinfo.username
$clientid = $appinfo.GetNetworkCredential().Password
$hostsToCheck = @()

#Get all the scopes we have available for this application
connect-mggraph -clientid $clientid -tenantid $tenantid -Scopes "ThreatHunting.Read.All"

# set the URL for the API
$url = "https://graph.microsoft.com/v1.0/security/runHuntingQuery"

# loop round the subnet list and ask for a list of computers
foreach ($subnet in $subnetList)
{
    # construct the query: 
    # Join tables so that we can make sure it's a windows client.
    # Project the informatio we want.
    # Order by timestamp to get the freshest value first.
    $query = '{ "Query" : "DeviceInfo | join kind=innerunique DeviceNetworkInfo on DeviceId | where OSPlatform contains \"Windows\" | where PublicIP startswith \"<SUBNET_HERE>\" | distinct DeviceId, DeviceName, Timestamp, PublicIP | order by Timestamp" }'
    
    # Replace the subnet with the one we're interested in searching for.
    $query = $query.Replace("<SUBNET_HERE>",$subnet)
    
    # Make the request
    $result = Invoke-GraphRequest -Uri $url -Method POST -ContentType "application/json" -Body $query

    # Value that we'll use in the while loop
    $hostToCheck = ""

    # index value so that we don't go on forever
    $index = 0

    # loop through results until we find a host that's alive or exhaust our options
    while ($hostToCheck -eq "") 
    {
        if ($index -ge $result.results.Count)   # we've run out of hosts to check in this subnet
        {
            # run out of hosts to check in this subnet
            write-host "Can't find a host that is alive in" $subnet
            $hostToCheck = "noMoreHosts" # set this to get out of the while loop
        }
        elseif (Test-Connection $result.results[$index].DeviceName) # see if we can connect to the hosts
        {
            # set this to get out of the for loop
            $hostToCheck = $result.results[$index].DeviceName
            
            # add the host to the array that we'll output later
            $hostsToCheck += $result.results[$index].DeviceName
        }
        # increment the index so we test a new host
        $index++   
    }

}

# output to the file
$hostsToCheck | Out-File -Force -FilePath $outputFile

# send a return value of the number of hosts found. 
$hostsToCheck
