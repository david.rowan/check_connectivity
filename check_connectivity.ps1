<#
.SYNOPSIS
Test connection to multiple hosts and ports on tcp/udp/icmp as defined in a csv file

Skrevt av: David Rowan

.DESCRIPTION
Takes a csv file that must have columns headed IP, Port,Port Protocol.
Will give back a file with an additional header called "AccessibleFrom_<IP ADDRESS>"
this column will have either true or false (or perhaps error)
Output file is called "InputFilename_processed.csv"

.EXAMPLE
check_connectivity.ps1 -InputFile <INPUTFILENAME>
Checks connectivity from current computer to every host/port/protocol combination in Inputfile, outputs to Inputfile_processed
.EXAMPLE
check_connectivity.ps1 -InputFile <INPUTFILENAME> -overwrite True
if you want to overwrite the input file with the output file
.EXAMPLE
check_connectivity.ps1 -InputFile <INPUTFILENAME> -force True -outputfilename <OUTPUTFILENAME>
if you want you can force to overwrite any output file with the -force True switch, e.g.

.PARAMETER inputFile
The path to the input csv file

.PARAMETER outputFile
The path to the output file. If this isn't set the name of the input file is used with _processed before the csv extension

.PARAMETER overwrite
If set to True will overwrite the input file with the results

.PARAMETER force
If set to True will overwrite the output file even if it already exists

.PARAMETER header
Takes a string that will become the header of the new column in the CSV file

.INPUTS
Inputfile is required to be a csv file with at least three columns headed:
IP: the IP address (or hostname) of the target
Port: the port number that is to be checked
Port protocol: the protocol (currently works with tcp/udp/icmp)
This should work with the output you get from OpenVas when you select "CSV Results" format

.OUTPUTS
A csv file to the Outputfile parameter. This will be as the original with an additional column:
AccessibleFrom_<IP address the script was run from>
The new column will contain one of four values:
True: the host was contactable
False: the host was not contactable
Error: an error occurred
Can't test for <protocol>: the protocol was not one of the expected protocols. No test was carried out

#>
param ($inputFile, $outputFile, $overwrite, $force, $header) # these are my named command line argument


#
# want to keep this as one file but annoyingly have to have function before it is used. Pasting at top to make it easier(??????)
#
#
#
# From: https://learn-powershell.net/2011/02/21/querying-udp-ports-with-powershell/
function Test-Port {  
    <#    
.SYNOPSIS    
    Tests port on computer.  
     
.DESCRIPTION  
    Tests port on computer. 
      
.PARAMETER computer  
    Name of server to test the port connection on.
       
.PARAMETER port  
    Port to test 
        
.PARAMETER tcp  
    Use tcp port 
       
.PARAMETER udp  
    Use udp port  
      
.PARAMETER UDPTimeOut 
    Sets a timeout for UDP port query. (In milliseconds, Default is 1000)  
       
.PARAMETER TCPTimeOut 
    Sets a timeout for TCP port query. (In milliseconds, Default is 1000)
                  
.NOTES    
    Name: Test-Port.ps1  
    Author: Boe Prox  
    DateCreated: 18Aug2010   
    List of Ports: http://www.iana.org/assignments/port-numbers  
       
    To Do:  
        Add capability to run background jobs for each host to shorten the time to scan.         
.LINK    
    https://boeprox.wordpress.org 
      
.EXAMPLE    
    Test-Port -computer 'server' -port 80  
    Checks port 80 on server 'server' to see if it is listening  
     
.EXAMPLE    
    'server' | Test-Port -port 80  
    Checks port 80 on server 'server' to see if it is listening 
       
.EXAMPLE    
    Test-Port -computer @("server1","server2") -port 80  
    Checks port 80 on server1 and server2 to see if it is listening  
     
.EXAMPLE
    Test-Port -comp dc1 -port 17 -udp -UDPtimeout 10000
     
    Server   : dc1
    Port     : 17
    TypePort : UDP
    Open     : True
    Notes    : "My spelling is Wobbly.  It's good spelling but it Wobbles, and the letters
            get in the wrong places." A. A. Milne (1882-1958)
     
    Description
    -----------
    Queries port 17 (qotd) on the UDP port and returns whether port is open or not
        
.EXAMPLE    
    @("server1","server2") | Test-Port -port 80  
    Checks port 80 on server1 and server2 to see if it is listening  
       
.EXAMPLE    
    (Get-Content hosts.txt) | Test-Port -port 80  
    Checks port 80 on servers in host file to see if it is listening 
      
.EXAMPLE    
    Test-Port -computer (Get-Content hosts.txt) -port 80  
    Checks port 80 on servers in host file to see if it is listening 
         
.EXAMPLE    
    Test-Port -computer (Get-Content hosts.txt) -port @(1..59)  
    Checks a range of ports from 1-59 on all servers in the hosts.txt file      
             
#>  
    [cmdletbinding(  
        DefaultParameterSetName = '',  
        ConfirmImpact = 'low'  
    )]  
    Param(  
        [Parameter(  
            Mandatory = $True,  
            Position = 0,  
            ParameterSetName = '',  
            ValueFromPipeline = $True)]  
        [array]$computer,  
        [Parameter(  
            Position = 1,  
            Mandatory = $True,  
            ParameterSetName = '')]  
        [array]$port,  
        [Parameter(  
            Mandatory = $False,  
            ParameterSetName = '')]  
        [int]$TCPtimeout = 1000,  
        [Parameter(  
            Mandatory = $False,  
            ParameterSetName = '')]  
        [int]$UDPtimeout = 1000,             
        [Parameter(  
            Mandatory = $False,  
            ParameterSetName = '')]  
        [switch]$TCP,  
        [Parameter(  
            Mandatory = $False,  
            ParameterSetName = '')]  
        [switch]$UDP                                    
    )  
    Begin {  
        If (!$tcp -AND !$udp) { $tcp = $True }  
        #Typically you never do this, but in this case I felt it was for the benefit of the function  
        #as any errors will be noted in the output of the report          
        $ErrorActionPreference = "SilentlyContinue"  
        $report = @()  
    }  
    Process {     
        ForEach ($c in $computer) {  
            ForEach ($p in $port) {  
                If ($tcp) {    
                    #Create temporary holder   
                    $temp = "" | Select-object Server, Port, TypePort, Open, Notes  
                    #Create object for connecting to port on computer  
                    $tcpobject = new-Object system.Net.Sockets.TcpClient  
                    #Connect to remote machine's port                
                    $connect = $tcpobject.BeginConnect($c, $p, $null, $null)  
                    #Configure a timeout before quitting  
                    $wait = $connect.AsyncWaitHandle.WaitOne($TCPtimeout, $false)  
                    #If timeout  
                    If (!$wait) {  
                        #Close connection  
                        $tcpobject.Close()  
                        Write-Verbose "Connection Timeout"  
                        #Build report  
                        $temp.Server = $c  
                        $temp.Port = $p  
                        $temp.TypePort = "TCP"  
                        $temp.Open = "False"  
                        $temp.Notes = "Connection to Port Timed Out"  
                    }
                    Else {  
                        $error.Clear()  
                        $tcpobject.EndConnect($connect) | out-Null  
                        #If error  
                        If ($error[0]) {  
                            #Begin making error more readable in report  
                            [string]$string = ($error[0].exception).message  
                            $message = (($string.split(":")[1]).replace('"', "")).TrimStart()  
                            $failed = $true  
                        }  
                        #Close connection      
                        $tcpobject.Close()  
                        #If unable to query port to due failure  
                        If ($failed) {  
                            #Build report  
                            $temp.Server = $c  
                            $temp.Port = $p  
                            $temp.TypePort = "TCP"  
                            $temp.Open = "False"  
                            $temp.Notes = "$message"  
                        }
                        Else {  
                            #Build report  
                            $temp.Server = $c  
                            $temp.Port = $p  
                            $temp.TypePort = "TCP"  
                            $temp.Open = "True"    
                            $temp.Notes = ""  
                        }  
                    }     
                    #Reset failed value  
                    $failed = $Null      
                    #Merge temp array with report              
                    $report += $temp  
                }      
                If ($udp) {  
                    #Create temporary holder   
                    $temp = "" | Select Server, Port, TypePort, Open, Notes                                     
                    #Create object for connecting to port on computer  
                    $udpobject = new-Object system.Net.Sockets.Udpclient
                    #Set a timeout on receiving message 
                    $udpobject.client.ReceiveTimeout = $UDPTimeout 
                    #Connect to remote machine's port                
                    Write-Verbose "Making UDP connection to remote server" 
                    $udpobject.Connect("$c", $p) 
                    #Sends a message to the host to which you have connected. 
                    Write-Verbose "Sending message to remote host" 
                    $a = new-object system.text.asciiencoding 
                    $byte = $a.GetBytes("$(Get-Date)") 
                    [void]$udpobject.Send($byte, $byte.length) 
                    #IPEndPoint object will allow us to read datagrams sent from any source.  
                    Write-Verbose "Creating remote endpoint" 
                    $remoteendpoint = New-Object system.net.ipendpoint([system.net.ipaddress]::Any, 0) 
                    Try { 
                        #Blocks until a message returns on this socket from a remote host. 
                        Write-Verbose "Waiting for message return" 
                        $receivebytes = $udpobject.Receive([ref]$remoteendpoint) 
                        [string]$returndata = $a.GetString($receivebytes)
                        If ($returndata) {
                            Write-Verbose "Connection Successful"  
                            #Build report  
                            $temp.Server = $c  
                            $temp.Port = $p  
                            $temp.TypePort = "UDP"  
                            $temp.Open = "True"  
                            $temp.Notes = $returndata   
                            $udpobject.close()   
                        }                       
                    }
                    Catch { 
                        If ($Error[0].ToString() -match "\bRespond after a period of time\b") { 
                            #Close connection  
                            $udpobject.Close()  
                            #Make sure that the host is online and not a false positive that it is open 
                            If (Test-Connection -comp $c -count 1 -quiet) { 
                                Write-Verbose "Connection Open"  
                                #Build report  
                                $temp.Server = $c  
                                $temp.Port = $p  
                                $temp.TypePort = "UDP"  
                                $temp.Open = "True"  
                                $temp.Notes = "" 
                            }
                            Else { 
                                <# 
                                It is possible that the host is not online or that the host is online,  
                                but ICMP is blocked by a firewall and this port is actually open. 
                                #> 
                                Write-Verbose "Host maybe unavailable"  
                                #Build report  
                                $temp.Server = $c  
                                $temp.Port = $p  
                                $temp.TypePort = "UDP"  
                                $temp.Open = "False"  
                                $temp.Notes = "Unable to verify if port is open or if host is unavailable."                                 
                            }                         
                        }
                        ElseIf ($Error[0].ToString() -match "forcibly closed by the remote host" ) { 
                            #Close connection  
                            $udpobject.Close()  
                            Write-Verbose "Connection Timeout"  
                            #Build report  
                            $temp.Server = $c  
                            $temp.Port = $p  
                            $temp.TypePort = "UDP"  
                            $temp.Open = "False"  
                            $temp.Notes = "Connection to Port Timed Out"                         
                        }
                        Else {                      
                            $udpobject.close() 
                        } 
                    }     
                    #Merge temp array with report              
                    $report += $temp 
                }                                  
            }  
        }                  
    }  
    End {  
        #Generate Report  
        $report
    }
}
#
#
#
#
#
#

# check version as it doesn't work in version 5...
if ($PSVersionTable.PSVersion.Major -eq 5)
{
    Write-host "Please user version 7 of powershell."
    Write-host "Start menu and pwsh to get version 7 to start"
    Write-host "If you need to install, see https://learn.microsoft.com/en-us/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.3"
    exit
}
if (-Not ($inputFile))
{
    write-host "No input file specified. Use -inputfile"
    exit
}
elseif (-not (Test-Path -Path $inputFile -PathType Leaf)) {
    Write-host "$inputFile doesn't exist. Exiting."
    exit
}

if (-Not ($outputFile)) # no output file specified. Make a default
{
    $outputFile = $inputFile.Substring(0, $inputFile.Length - 4) + "_processed.csv"
}
if ($overwrite) {
    $outputFile = $inputFile
}

# set timeouts
$timeout = 1
$udpTimeout = $timeout * 1000   # udp timeout is in milliseconds

# get the ip address we'll be running this from to put in the csv header
$myIP = test-netconnection | select-object sourceaddress
$myHeader = ""
if (-Not ($header)) {
    $myHeader = "AccessibleFrom_" + $myIP.SourceAddress.IPAddress
}
else {
    $myHeader = $header
}
# import the data from the inputfile
$data = Import-Csv $inputFile

# hash array for whether we have already checked a host.
$checkedAlreadyTCP = @{}
$checkedAlreadyUDP = @{}
$checkedAlreadyICMP = @{}

# loop round each line of the file
for ($i = 0; $i -lt $data.Length; $i++) 
{
    
    write-host "Checking " $data[$i].IP
    $result = ""
    if ($data[$i].IP -ne "") # only do this if the IP address is non-blank
    {
        try {
            if ($data[$i].'port protocol' -eq "tcp") 
            {
                if (Test-Path variable:checkedAlreadyTCP[$data[$i].IP][$data[$i].'port'])
                {
                    # we've already checked this one...
                    $result = $checkedAlreadyTCP[$data[$i].IP][$data[$i].'port']
                }
                else
                {
                    if (Test-Path variable:checkedAlreadyTCP[$data[$i].'ip'])
                    {
                        $checkedAlreadyTCP[$data[$i].'ip'][$data[$i].'port'] = $result
                    }
                    else 
                    {
                        $result = Test-Connection -TargetName $data[$i].IP -TcpPort $data[$i].port -Quiet -TimeoutSeconds $timeout
                        $checkedAlreadyTCP[$data[$i].'ip'] = @{}
                        $checkedAlreadyTCP[$data[$i].'ip'][$data[$i].'port'] = $result
                    }
                }
            }
            elseif (($data[$i].'port protocol' -eq "icmp") -or ($data[$i].'port protocol' -eq ""))
            {
                if (Test-Path variable:checkedAlreadyICMP[$data[$i].'ip'])
                {
                    $result = $checkedAlreadyICMP[$data[$i].'ip']
                }
                else 
                {
                    # sometimes this throws an exception that isn't caught for some reason. Consider it a work in progress.
                    # Yes, count should probably be more than 1 but this makes it more of a sane runtime.
                    $result = Test-Connection -TargetName $data[$i].IP -IPv4 -Quiet -TimeoutSeconds $timeout -Count 1
                    $checkedAlreadyICMP[$data[$i].'ip'] = $result
                }
                
            }
            elseif ($data[$i].'port protocol' -eq "udp") 
            {
                if (Test-Path variable:checkedAlready[$data[$i]][$data[$i].'port protocol'][$data[$i].'port']) {
                    # we've already checked this one...
                    $result = $checkedAlready[$data[$i]][$data[$i].'port protocol'][$data[$i].'port']
                }
                else 
                {
                    # need to work out testing a udp connection
                    $result = Test-Port -computer $data[$i].IP -port $data[$i].port -UDP -UDPtimeout $udpTimeout
                    $result = $result.Open
                    if (Test-Path variable:checkedAlreadyUDP[$data[$i].'ip'])
                    {
                        $checkedAlreadyUDP[$data[$i].'ip'][$data[$i].'port'] = $result
                    }
                    else 
                    {
                        $checkedAlreadyUDP[$data[$i].'ip'] = @{}
                        $checkedAlreadyUDP[$data[$i].'ip'][$data[$i].'port'] = $result
                    }
                }
            }   
            else 
            {
                $result = "can't test for " + $data[$i].'port protocol'
            }
        }
        catch {
            $result = "Error $_"
        }
        # $data[$i]
        # $myHeader
        # $result
        $data[$i] | Add-Member -MemberType NoteProperty -Name $myHeader -Value $result
    }

}

# deal with writing the file out - either force or check that the file exists.
if ($force)
{
    $data | Export-Csv -Path $outputFile -Force
}
else 
{
    if (-Not(Test-Path -Path $outputFile -PathType Leaf)) 
    {
        $data | Export-Csv -Path $outputFile
    }
    else {
        write-host "$outputFile already exists. Nothing written. Use -Force True if you want to overwrite or pick another output file with -outputfile 'filename'"
    }
}


