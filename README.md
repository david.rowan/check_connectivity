**SYNOPSIS**

Project to check whether found vulnerabilities are accessible from various points on the UiB network.
Generates a list of computers on the different subnets we're interested in.
Uses that list and the list of vulnerabilities to check whether the vulnerabilities are accessible from those computers.

**By**

David Rowan

**TODO**

- Process_wrapper: add working directory to make sure all files are kept where they are expected
- Cleanup: add working directory and sync this to the one in process_wrapper (perhaps a . include in each one?)
- Create script specific readme files

**OVERALL PROCESS DESCRIPTION**
```mermaid
flowchart TD
    one[Get a list of computers on different subnets on p1-admin-sec]
    two[Get vulnerability csv file from panopticon]
    three[Copy list of computers and vulnerabilities to p1-admin-brita]
    four[run process_wrapper.ps1 on p1-admin-brita]
    five[Copy final results file to safe]
    six[Run cleanup.ps1 on p1-admin-brita]
    one-->two
    two-->three
    three-->four
    four-->five
    five-->six

```

 - Step 1 (from _P1-admin-sec_):
   Elevate privileges to `security administrator`
   Go to the working directory (probably check_connectivity)
   Run `getListOfComputers.ps1` (with relevant options if needed).
   Copy `listOfComputers.txt` to the working directory on p1-admin-brita
 - Step 2 (on _p1-admin-brita_):
   Get the vulnerability csv file into the working directory on p1-admin-brita. Make sure it is called `input0.csv`
   Run `process_wrapper.ps1` (with relevant options if needed)
   - monitor output to see if any computers couldn't be connected to.
   Copy `finalResults.csv` to safe where they can be onward processed by Rafael's scripts
   Verify that they are what you expect them to be!
 - Step 3 (on _p1-admin-brita_):
   Run cleanup.ps1 to remove the vulnerability files from p1-admin-brita
Takes a csv file that must have columns headed IP, Port,Port Protocol.
Will give back a file with an additional header called "AccessibleFrom_<IP ADDRESS>"
this column will have either true or false (or perhaps error)
Output file is called "InputFilename_processed.csv"

**Limitations**

If the input csv file is too big then the whole process fails. 5000 lines is certaily too big so break it up to smaller files. 2000 lines seems to be fine but it's really about object/file size rather than line count. If this is the case use split_csv_file.ps1 to split your file easily.

**EXAMPLE (getListOfComputers)**

Requires an appinfo.xml auth file ("generate with Get-Credential | Export-CliXML" with tennantID as username and appID as password)
.\getListOfComputers.ps1 
Optionally add -inputFile for an alternative location of the subnet list.
Optionally add -outputFile for an alternative location to save the output file.

**EXAMPLE (process_wrapper)**

.\process_wrapper.ps1
Optionally add -computerNameFile for a different location of computernames.
Optionally add -outpufFile for a different locaiton to store the final results file.
- do not add a -vulnerabilityFile option unless you change the script.

**EXAMPLE (cleanup)**

.\cleanup.ps1
No further options necessary. Will sdelete all .csv and listOfComputers.txt files from the working directory
