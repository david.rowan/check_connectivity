<#
.SYNOPSIS
Cleanup after running the scripts to make sure we don't leave any sensitive data on p1-admin-brita

.DESCRIPTION
uses sdelete to delete all csv files in the working directory and "listOfComputers.txt"

.EXAMPLE
.\cleanup.ps1

#>
# delete the csv files that we no longer need.
sdelete -nobanner *.csv

# delete the list of computers
sdelete -nobanner listOfComputers.txt