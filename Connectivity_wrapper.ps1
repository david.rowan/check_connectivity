# example of how to run this on remote computers...


$myTestInput = Import-Csv -Path "exampleCSVFile.csv"

# update the URL here for the final version
$myCommand = (New-Object System.Net.WebClient).DownloadString('https://git.app.uib.no/david.rowan/check_connectivity/-/raw/main/check_connectivity_function_for_remote.ps1?inline=false')

# get the results into a variable
$myResult = Invoke-Command -ComputerName uib3104275.klient.uib.no -scriptblock { Invoke-Expression $using:myCommand; $result = Test-Many-ports -inputObject $using:myTestInput; return $result}


# save them to a csv if required
#$myResult | Export-csv -Path "results.csv"